package javaapplication6_mario;

import java.util.Random;
import jenetic.interfaces.IGene;

/**
 * Created by LAPIS
 */
public class Gene1D implements IGene {

    private int gene;
    private Random rnd = new Random();


    /**
     * Generates a new IGene with the provided information
     *
     * @param gene
     */
    public Gene1D(int gene) {
        this.gene = gene;
    }

    /**
     * Generates a new random IGene within the bounds of the solution space
     */
    public Gene1D() {
        int num = rnd.nextInt(25);
        if (num > 2) {
            this.gene = 4;
        } else {
            this.gene = 3;
        }
    }

    @Override
    protected Object clone() {
        return new Gene1D(this.gene);
    }

    @Override
    public String toString() {
        return gene + "";
    }

    public int getGene() {
        return gene;
    }

    public void setGene(int gene) {
        this.gene = gene;
    }

    @Override
    public boolean isValid() {
        return true;
    }
}
