/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6_mario;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;
import java.util.Random;
import java.util.Scanner;
import luigi.*;

/**
 *
 * @author LAPIS
 */
public class JavaApplication6_mario {

    //public static String ip = "172.20.130.110";
    //public static String ip = "192.168.1.52";
    public static String ip = "172.20.131.89";

    public static void main(String[] args) throws IOException {

        //findSolutionRandomF(1); 
        //solutionMaxTesteF(1);
        //randomSolution();
        //genesSolutions();
        HundredSolutions();
        //readandsave();
        //justRun();
        //Mario_Save_To_DocTexto(new Integer[]{3}, ".\\files\\save.txt");
    }

    public static void randomSolution() throws IOException {

        Random rnd = new Random();

        Integer[] solution;

        //carregar array
        solution = Mario_Load_From_DocTexto(".\\files\\solution.txt");

        //correr solucao guardada
        Request req = new Request(solution, "SuperMarioBros-v0", "true", "forever");

        MarioUtils mu = new MarioUtils(ip);
        System.out.println("Request made!");
        RunResult rr = mu.goMarioGo(req);

        System.out.println("Resultados:\n" + rr.toString());

        Integer[] solution2 = null;

        /*
        tendo em conta a solucao guardada,
        adiciona comandos, se reason_finish='no_more_commands'
        apaga comandos, se reason_finish='death'
         */
        if (rr.getReason_finish().equals("no_more_commands")) {

            System.out.println("Need more commands, tem " + solution.length + " comandos");

            //aumentar o array
            solution2 = new Integer[solution.length + 30];

            //copiar array
            int n = 0;
            for (Integer integer : solution) {
                solution2[n] = integer;
                n++;
            }

            //adicionar comandos
            for (int i = solution.length; i < solution2.length; i++) {
                int num = rnd.nextInt(25);
                if (num > 2) {
                    solution2[i] = 4;
                } else {
                    solution2[i] = 3;
                }

            }
            System.out.println("More commands added, agora tem " + solution2.length + " comandos");
        } else if (rr.getReason_finish().equals("death")) {
            System.out.println("Need less commands, tem " + solution.length + " comandos");

            //remover comandos
            solution2 = new Integer[solution.length - (rnd.nextInt(40) + 10)];

            //copiar array
            for (int i = 0; i < solution2.length; i++) {
                solution2[i] = solution[i];
            }
            System.out.println("Commands removed, agora tem " + solution2.length + " comandos");

        }

        //salvar array
        Mario_Save_To_DocTexto(solution2, ".\\files\\solution.txt");

        //mu.submitToLeaderboard(rr, "Luigi", "forever");
    }

    public static void genesSolutions() throws IOException {

        Gene1D g3 = new Gene1D(3);
        Gene1D g4 = new Gene1D(4);
        Chromosome1D c1 = new Chromosome1D(100, 200);

        System.out.println(c1.toString());

        Integer[] solution;
        Random rnd = new Random();

        //carregar array
        solution = Mario_Load_From_DocTexto(".\\files\\solution.txt");

        //correr solucao guardada
        Request req = new Request(solution, "SuperMarioBros-v0", "true", "forever");

        MarioUtils mu = new MarioUtils(ip);
        System.out.println("Request made!");
        RunResult rr = mu.goMarioGo(req);

        System.out.println("Resultados:\n" + rr.toString());

        Integer[] solution2 = null;

        //tendo em conta a solucao guardada,
        //adiciona comandos, se reason_finish='no_more_commands'
        //apaga comandos, se reason_finish='death'
        if (rr.getReason_finish().equals("no_more_commands")) {

            System.out.println("Need more commands, tem " + solution.length + " comandos");

            //aumentar o array
            solution2 = new Integer[solution.length + c1.getSize()];

            //copiar array
            int n = 0;
            for (Integer integer : solution) {
                solution2[n] = integer;
                n++;
            }

            //adicionar comandos
            int j = 0;
            for (int i = solution.length; i < solution2.length; i++) {
                solution2[i] = ((Gene1D) c1.getGenes().get(j)).getGene();
                j++;
            }
            System.out.println("More commands added, agora tem " + solution2.length + " comandos");
        } else if (rr.getReason_finish().equals("death")) {
            System.out.println("Need less commands, tem " + solution.length + " comandos");

            //remover comandos
            solution2 = new Integer[solution.length - (rnd.nextInt(80) + 20)];

            //copiar array
            for (int i = 0; i < solution2.length; i++) {
                solution2[i] = solution[i];
            }
            System.out.println("Commands removed, agora tem " + solution2.length + " comandos");

        }
        //salvar array
        Mario_Save_To_DocTexto(solution2, ".\\files\\solution.txt");

    }

    /**
     * Este metodo carrega a solucao guardade ate agora e testa 100 vezes uma
     * array de 500 comandos/genes e no final de X iteracoes apenas os primeiros
     * 450 comandos/genes desse array, com o melhor reward de todas as
     * iteracoes, sao adicionados à solucao e guardados no ficheiro
     *
     * @throws IOException
     */
    public static void HundredSolutions() throws IOException {

        //carregar solucao guardada ate ao momento
        Integer[] solution = Mario_Load_From_DocTexto(".\\files\\solution100.txt");

        //correr solucao 
        System.out.println("Correr Solução Atual");
        Request req = new Request(solution, "SuperMarioBros-v0", "true", "forever");

        MarioUtils mu = new MarioUtils(ip);
        System.out.println("Request made!");
        RunResult rr = mu.goMarioGo(req);

        System.out.println("Resultados:\n" + rr.toString());

        //guarda o reward da solucao atual
        int reward = rr.getReward();

        //menu
        //1-adicionar 450 comandos/genes e entre 1 e 500 iteracoes
        //2-remover 450 comandos/genes adicionar outros 450 comando/genes apos entre 1 e 500 iteracoes
        //0-sair do programa
        Scanner in = new Scanner(System.in);
        String s = "", v = "";
        int iteracoes = 0, tamanho = 0;

        do {
            System.out.println("\u001B[34m 1 -\u001B[0m Adicionar mais 450 comandos/genes e tentar X iteracoes");
            System.out.println("\u001B[34m 2 -\u001B[0m Remover os ultimos 450 comandos/genes e tentar X iteraçoes");
            System.out.println("\u001B[34m 0 -\u001B[0m Sair");
            System.out.print("\u001B[0m Opção: \u001B[31m");
            s = in.nextLine();
            if (!"1".equals(s) && !"2".equals(s) && !"0".equals(s)) {
                System.out.println("\u001B[31m Opção Inválida, por favor introduza uma opçao válida! (1, 2, 0)");
            } else if ("1".equals(s) || ("2".equals(s) && solution.length > 450)) {
                if ("2".equals(s)) {
                    System.out.println("\u001B[35m 450 comandos/genes removidos \u001B[0m");

                    tamanho = solution.length - 450;
                } else {
                    tamanho = solution.length;
                }

                //escolher quantidade de iteracoes
                do {
                    System.out.print("\u001B[0m Iterações [Entre 1-500]: \u001B[31m");
                    v = in.nextLine();
                    try {
                        iteracoes = Integer.valueOf(v);
                    } catch (Exception e) {
                        iteracoes = -5;
                    }
                    if (!(iteracoes >= 1 && iteracoes <= 500)) {
                        System.out.println("\u001B[31m Valor Inválido, por favor introduza um valor válido! (1 a 500)");
                    }
                } while (!(iteracoes >= 1 && iteracoes <= 500));
                System.out.println("\u001B[35m Iniciar " + v + " iterações... \u001B[0m");

                //ciclo de X iteracoes
                ciclo(tamanho, iteracoes, solution, reward);

            } else if ("2".equals(s) && solution.length <= 450) {

                System.out.println("\u001B[31m Solução atual não possui o minimo de 450 comandos/genes, por favor selecione opção 1 ou 0.");

            } else if ("0".equals(s)) {
                System.out.println("\u001B[31m Sair! Bye Bye !!!");
                System.exit(0);
            }
        } while (!"1".equals(s) && !"2".equals(s) && !"0".equals(s));

        System.out.println("Fim do Programa!");

    }

    public static void ciclo(int tamanho, int iteracoes, Integer[] solution, int reward) throws IOException {
        //array que vai ter a melhor solucao
        Integer[] boa = new Integer[tamanho + 450];

        //iteracao para procurar o melhor conjunto de comandos/genes que da mais reward
        for (int i = 0; i < iteracoes; i++) {
            System.out.println("\nTentativa: " + (i + 1) + "/" + iteracoes);

            //gerar cromossoma aleatorio com 500 comandos/genes
            Chromosome1D c1 = new Chromosome1D(500);

            //novo array com mais 1000 espaços
            Integer[] solution2 = new Integer[tamanho + 500];

            //copiar solucao guardada
            for (int j = 0; j < tamanho; j++) {
                solution2[j] = solution[j];
            }

            //adicionar cromossoma aleatorio com 500 comandos/genes no array atual
            int k = 0;
            for (int j = tamanho; j < solution2.length; j++) {
                solution2[j] = ((Gene1D) c1.getGenes().get(k)).getGene();
                k++;
            }

            //correr solucao 
            Request req = new Request(solution2, "SuperMarioBros-v0", "true", "forever");

            MarioUtils mu = new MarioUtils(ip);
            System.out.println("Request made!");
            RunResult rr = mu.goMarioGo(req);

            System.out.println("Resultados:\n" + rr.toString());

            if (rr.getReward() > reward && !rr.getReason_finish().equals("death")) {
                boa = new Integer[tamanho + 450];
                for (int j = 0; j < boa.length && j < solution2.length; j++) {
                    boa[j] = solution2[j];
                }
                reward = rr.getReward();

                //salvar array
                Mario_Save_To_DocTexto(boa, ".\\files\\solution100.txt");
            } else if (rr.getReward() > reward && rr.getReason_finish().equals("death")) {
                boa = new Integer[rr.getCommands_used() - 50];
                for (int j = 0; j < rr.getCommands_used() - 50; j++) {
                    boa[j] = solution2[j];
                }
                reward = rr.getReward();

                //salvar array
                Mario_Save_To_DocTexto(boa, ".\\files\\solution100.txt");
            }
            System.out.println("\u001B[31m Reward: " + reward);
        }
    }

    public static void justRun() throws IOException {
        //carregar solucao guardada
        Integer[] solution = Mario_Load_From_DocTexto(".\\files\\solution100.txt");

        //correr solucao 
        Request req = new Request(solution, "SuperMarioBros-v0", "true", "forever");

        MarioUtils mu = new MarioUtils(ip);
        System.out.println("Request made!");
        RunResult rr = mu.goMarioGo(req);

        System.out.println("Resultados:\n" + rr.toString());
    }

    public static void readandsave() throws IOException {
        Integer[] solution = Mario_Load_From_DocTexto(".\\files\\solution100.txt");

        Mario_Save_To_DocTexto(solution, ".\\files\\save.txt");
    }

    public static void Mario_Check_DocTexto() {
        File file = new File(".\\files\\solutions.txt");
        final Formatter x;

        if (file.exists()) {
            System.out.println("File_Texto_Existes");
        } else {
            System.out.println("File_Doesn't_Texto_Existes");
            try {
                x = new Formatter(".\\files\\solutions.txt");
                System.out.println("Sucess_Create_Texto");
            } catch (IOException e) {
                System.err.println("Erro_Create_Texto!");
            }
        }
    }

    public static void Mario_Save_To_DocTexto(Integer[] solution, String file_path) throws IOException {
        File file = new File(file_path);
        FileWriter fw = new FileWriter(file);
        String string = "";
        try {
            for (Integer integer : solution) {
                string = string.concat(integer + " ");
            }
            fw.write(string + " ");
            fw.flush();
            fw.close();
            System.out.println("Sucess_Save_Texto");
        } catch (IOException e) {
            System.err.println("Erro_Save_Texto!");
        }
    }

    public static Integer[] Mario_Load_From_DocTexto(String path_file) throws IOException {
        File file = new File(path_file);
        FileReader fr = new FileReader(file);
        Integer[] sol = null;
        int tamanho = 0;
        int num;
        try {
            int i;
            int j = 0;
            while ((i = fr.read()) != -1) {
                num = Character.getNumericValue((char) i);
                if (num >= 0 && num < 10) {
                    tamanho++;
                }
            }
            fr = new FileReader(file);
            sol = new Integer[tamanho];
            while ((i = fr.read()) != -1) {
                num = Character.getNumericValue((char) i);
                if (num >= 0 && num < 10) {
                    sol[j] = Character.getNumericValue((char) i);
                    j++;
                }
            }
            System.out.println("Sucess_Read_Texto");
        } catch (IOException e) {
            System.err.println("Erro_Read_Texto!");
        }
        return sol;
    }

}
