package Nivel;

import jenetic.interfaces.IChromosome;
import jenetic.interfaces.IGene;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Created by LAPIS
 */
public class Chromosome1D implements IChromosome, Comparable<IChromosome> {

    private int minSize;
    private int maxSize;
    private int size;
    private List<IGene> genes;

    /**
     * Generates a new random solution
     *
     * @param maxSize
     * @param minSize
     */
    public Chromosome1D(int minSize, int maxSize) {
        this.minSize = minSize;
        this.maxSize = maxSize;

        this.size = new Random().nextInt(maxSize + 1 - minSize) + minSize;
        genes = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            genes.add(new Gene1D());
        }
    }

    public Chromosome1D(int size) {
        this.size = size;
        genes = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            genes.add(new Gene1D());
        }
    }
    
    private Chromosome1D(Chromosome1D other, IGene gene) {
        this.minSize = other.minSize;
        this.maxSize = other.maxSize;
        this.genes = new ArrayList<>();
        this.genes.add(gene);
    }

    @Override
    public double getFitness() {
        Gene1D g = (Gene1D) genes.get(0);
        double x = g.getGene();
        return 2 * sin(x) - 1 * cos(x);//nao sei
    }

    public Chromosome1D(Chromosome1D other) {
        this.minSize = other.minSize;
        this.maxSize = other.maxSize;
        this.genes = other.getGenes();
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public boolean isValid(IChromosome ch) {
        return true;
    }

    @Override
    public IChromosome mutate(double mr) {
        Gene1D gene = (Gene1D) ((Gene1D) this.genes.get(0)).clone();
        Chromosome1D novo;
        int limit = 0;
        do {
            double dif = mr * 0.5 * new Random().nextDouble();
            if (new Random().nextDouble() > 0.5) {
                dif *= -1;
            }

            gene.setGene((int) (gene.getGene() + dif));
            novo = new Chromosome1D(this, gene);

        } while (!isValid(novo) && limit++ < 30);

        return novo;
    }

    @Override
    public IChromosome cross(IChromosome other) {
        Gene1D gene = (Gene1D) ((Gene1D) this.genes.get(0)).clone();
        Chromosome1D novo;
        int limit = 0;
        do {
            gene.setGene((((Gene1D) other.getGenes().get(0)).getGene() + gene.getGene()) / 2);
            novo = new Chromosome1D(this, gene);
        } while (!isValid(novo) && limit++ < 30);

        return novo;
    }

    @Override
    public IChromosome heredity() {
        return new Chromosome1D(this);
    }

    @Override
    protected Object clone() {
        return new Chromosome1D(this);
    }

    @Override
    public String toString() {
        String s = "";
        for (IGene gene : genes) {
            s = s.concat(gene + ", ");
        }
        return s;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    @Override
    public List<IGene> getGenes() {
        return genes;
    }

    public void setGenes(List<IGene> genes) {
        this.genes = genes;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int compareTo(IChromosome o) {
        return Double.compare(this.getFitness(), o.getFitness());
    }
}
