package javaapplication7_mario;

import java.util.Random;
import jenetic.interfaces.IGene;

/**
 * Created by LAPIS.
 */
public class Gene implements IGene {

    private int gene;
    private Random rnd = new Random();

    /**
     * Generates a new IGene with the provided information
     *
     * @param gene
     */
    public Gene(int gene) {
        this.gene = gene;
    }

    /**
     * Generates a new IGene with the gene
     *
     * @param gene
     */
    public Gene(Gene gene) {
        this.gene = gene.getGene();
    }

    /**
     * Generates a new random IGene within the bounds of the solution space
     */
    public Gene() {
        double num = rnd.nextDouble();
        if (num < 0.2) {
            this.gene = 3;
        } else {
            this.gene = 4;
        }
    }

    @Override
    protected Object clone() {
        return new Gene(this.gene);
    }

    @Override
    public String toString() {
        return gene + "";
    }

    public int getGene() {
        return gene;
    }

    public void setGene(int gene) {
        this.gene = gene;
    }

    @Override
    public boolean isValid() {
        if (gene >= 0 && gene >= 11) {
            return true;
        } else {
            return false;
        }
    }
}
