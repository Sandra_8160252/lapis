package javaapplication7_mario;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import jenetic.interfaces.IChromosome;
import jenetic.interfaces.IJenetic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import luigi.MarioUtils;
import luigi.Request;
import luigi.RunResult;

/**
 * Created by LAPIS.
 */
public class Jenetic implements IJenetic {

    private int populationSize;
    private int crossoverSize;
    private int mutationSize;
    private int maxIterations;
    private int randomSize;
    private int tamanhoBest;
    private double deltaMin;
    private double mutationFactor;
    private int iteration;
    private List<Cromossoma> populacao;

    public Jenetic(int populationSize, int crossoverSize, int mutationSize, int maxIterations, double deltaMin, double mutationFactor) {
        this.populationSize = populationSize;
        this.crossoverSize = crossoverSize;
        this.mutationSize = mutationSize;
        this.maxIterations = maxIterations;
        this.deltaMin = deltaMin;
        this.mutationFactor = mutationFactor;
        this.iteration = 0;
        this.tamanhoBest = 0;
        this.populacao = new ArrayList();
    }

    //cria populacao
    @Override
    public List<IChromosome> initialize() {

        List<IChromosome> cromos = new ArrayList();

        for (int i = 1; i <  populationSize; i++) {
            this.populacao.add(new Cromossoma(tamanhoBest, tamanhoBest + 1000));
        }

        for (int i = 0; i < populationSize; i++) {
            cromos.add(this.populacao.get(i));
        }
        return cromos;
    }

    //escolhe um cromossoma aleatorio
    private IChromosome getRandomChromosome(List<IChromosome> parents) {
        return parents.get(new Random().nextInt(parents.size()));
    }

    //faz cross
    @Override
    public List<IChromosome> cross(List<IChromosome> parents) {
        return Stream.generate(() -> getRandomChromosome(parents).cross(getRandomChromosome(parents)))
                .limit(crossoverSize)
                .collect(Collectors.toList());
    }

    //
    @Override
    public List<IChromosome> random() {
        return Stream.generate(() -> ((IChromosome) new Cromossoma(1, 1)))
                .limit(randomSize)
                .collect(Collectors.toList());
    }

    @Override
    public List<IChromosome> mutate(List<IChromosome> parents) {
        //    return Stream.generate(() -> getRandomChromosome(parents).mutate(1)) .limit(mutationSize)  .collect(Collectors.toList());
        return parents;
    }

    @Override
    public List<IChromosome> heredity(List<IChromosome> parents) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<IChromosome> selection(List<IChromosome> parents) {
        return parents.stream().sorted((x, y) -> Double.compare(y.getFitness(), x.getFitness()))
                .limit(populationSize)
                .collect(Collectors.toList());
    }

    @Override
    public boolean canStop() {
        return iteration >= maxIterations;
    }

    @Override
    public int run() {
        int runs = 0;
        do {
            Integer[] solution;

            //meter solucoes na populacao
            //gerar populacao
            //meter a melhor solucao guardada na primeira posicao
                        
            try {
                solution = Mario_Load_From_DocTexto(".\\files\\solutionGenetica.txt");

                Cromossoma c = new Cromossoma(solution);

                this.populacao.add(c);
            } catch (Exception e) {
                System.out.println("Erro a carregar a solucao guardada!");
            }

            Request req = new Request(populacao.get(0).getArrayGene(), "SuperMarioBros-v0", "true", "forever");

            MarioUtils mu = new MarioUtils("172.20.131.89");
            System.out.println("Request made!");
            RunResult rr = mu.goMarioGo(req);

            System.out.println("Resultados:\n" + rr.toString());

            tamanhoBest = rr.getCommands_used() - 10;
            initialize();

            //correr os cromossomas desta geracao
            for (int i = 1; i < populacao.size(); i++) {
                req = new Request(populacao.get(i).getArrayGene(), "SuperMarioBros-v0", "true", "forever");

                mu = new MarioUtils("172.20.131.89");
                System.out.println("Request made!");
                rr = mu.goMarioGo(req);

                System.out.println("Resultados:\n" + rr.toString());

                //guardar o ressultado deste cromossoma
                populacao.get(i).setResultado(rr);
            }

            //getBestResultado
            IChromosome c1 = getBestSolution();
            try {
                Mario_Save_To_DocTexto(((Cromossoma) c1).getArrayGene(), ".//files//solutionGenetica.txt");
            } catch (IOException ex) {
                System.out.println("Erro a salvar");
            }
            //cros dessa solucao/cromossoma
            //mutacao
            //nova geracao com mutacao e crossss
            //corro toda a geracao
            //ver se os resultados aumentam e guardo a melhor solucao, senao volto a fazer mutacoes com a solucao que tenho guardada

            runs++;
        } while (runs < iteration);
        return iteration;
    }

    public void writeResults() {
        try {
            ArrayList<String> header = new ArrayList<>();
            header.add("Fitness");
            ArrayList<String> header2 = new ArrayList<>();
            header2.add("X");
            Files.deleteIfExists(Paths.get("fitnesses"));
            Files.deleteIfExists(Paths.get("avgFitnesses"));
            Files.deleteIfExists(Paths.get("coordinates"));
            Files.write(Paths.get("fitnesses"), header, StandardOpenOption.CREATE);
            Files.write(Paths.get("avgFitnesses"), header, StandardOpenOption.CREATE);
            Files.write(Paths.get("coordinates"), header2, StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IChromosome getFittest(List<IChromosome> population) {

        return population.stream().sorted((x, y) -> Double.compare(y.getFitness(), x.getFitness())).findFirst().get();
    }

    @Override
    public double getAverageFitness(List<IChromosome> population) {
        return population.stream().mapToDouble(IChromosome::getFitness).average().getAsDouble();
    }

    @Override
    public IChromosome getBestSolution() {
        int best = 0;
        IChromosome c = null;
        for (int i = 0; i < populacao.size(); i++) {
            if (populacao.get(i).getFitness() > best) {
                c = populacao.get(i);
            }
        }
        return c;
    }

    public static void Mario_Check_DocTexto() {
        File file = new File(".\\files\\solutions.txt");
        final Formatter x;

        if (file.exists()) {
            System.out.println("File_Texto_Existes");
        } else {
            System.out.println("File_Doesn't_Texto_Existes");
            try {
                x = new Formatter(".\\files\\solutions.txt");
                System.out.println("Sucess_Create_Texto");
            } catch (IOException e) {
                System.err.println("Erro_Create_Texto!");
            }
        }
    }

    public static void Mario_Save_To_DocTexto(Integer[] solution, String file_path) throws IOException {
        File file = new File(file_path);
        FileWriter fw = new FileWriter(file);
        String string = "";
        try {
            for (Integer integer : solution) {
                string = string.concat(integer + " ");
            }
            fw.write(string + " ");
            fw.flush();
            fw.close();
            System.out.println("Sucess_Save_Texto");
        } catch (IOException e) {
            System.err.println("Erro_Save_Texto!");
        }
    }

    public static Integer[] Mario_Load_From_DocTexto(String path_file) throws IOException {
        File file = new File(path_file);
        FileReader fr = new FileReader(file);
        Integer[] sol = null;
        int tamanho = 0;
        int num;
        try {
            int i;
            int j = 0;
            while ((i = fr.read()) != -1) {
                num = Character.getNumericValue((char) i);
                if (num >= 0 && num < 10) {
                    tamanho++;
                }
            }
            fr = new FileReader(file);
            sol = new Integer[tamanho];
            while ((i = fr.read()) != -1) {
                num = Character.getNumericValue((char) i);
                if (num >= 0 && num < 10) {
                    sol[j] = Character.getNumericValue((char) i);
                    j++;
                }
            }
            System.out.println("Sucess_Read_Texto");
        } catch (IOException e) {
            System.err.println("Erro_Read_Texto!");
        }
        return sol;
    }

}
