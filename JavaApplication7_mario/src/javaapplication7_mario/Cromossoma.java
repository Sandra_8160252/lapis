package javaapplication7_mario;

import jenetic.interfaces.IChromosome;
import jenetic.interfaces.IGene;

import java.util.*;
import java.util.stream.Collectors;
import luigi.RunResult;

/**
 * Created by LAPIS.
 */
public class Cromossoma implements IChromosome, Comparable<IChromosome> {

    private int maxSize;
    private int minSize;
    private List<IGene> genes;
    private RunResult resultado;

    /**
     * Generates a new random solution/cromossoma
     *
     * @param minSize - tamanho minimo do cromossoma
     * @param maxSize - tamanho maximo do cromossoma
     */
    public Cromossoma(int minSize, int maxSize) {
        this.minSize = minSize;
        this.maxSize = maxSize;

        int size = new Random().nextInt(maxSize + 1 - minSize) + minSize;
        this.genes = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            this.genes.add(new Gene());
        }
    }

    public Cromossoma(Integer[] array) {
        genes = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            genes.add(new Gene(array[i]));
        }
    }

    /**
     *
     * @param other
     * @param gene
     */
    private Cromossoma(Cromossoma other, IGene gene) {
        this.minSize = other.minSize;
        this.maxSize = other.maxSize;
        this.genes = new ArrayList<>();
        this.genes.add(gene);
    }

    /**
     *
     * @param other
     */
    public Cromossoma(Cromossoma other) {
        this.genes = new ArrayList<>();
        this.genes.addAll(other.genes.stream().map(x -> new Gene(((Gene) x))).collect(Collectors.toList()));
        this.resultado = other.getResultado();
    }

    @Override
    public double getFitness() {
        if (this.resultado != null) {
            return this.resultado.getReward();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isValid() {
        for (IGene gene : this.genes) {
            if (!gene.isValid()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isValid(IChromosome ch) {
        for (IGene gene : ch.getGenes()) {
            if (!gene.isValid()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public IChromosome mutate(double taxaMutacao) {
        Random rnd = new Random();
        Cromossoma novo = new Cromossoma(this);
        List<IGene> listaNova = null;

        //verifica se a taxa de mutacao esta entre 0 e 1
        //se a taxa de mutacao for 0, devolve o cromossoma igual
        if (taxaMutacao > 1) {
            taxaMutacao = taxaMutacao % 1;
        } else if (taxaMutacao < 0) {
            taxaMutacao = -taxaMutacao % 1;
        } else if (taxaMutacao == 0) {
            return this;
        }

        //gera um numero aleatorio entre 0 e 1, se o numero for menor que a taxaMutacao o gene é alterado
        for (IGene gene : this.genes) {
            if (rnd.nextDouble() < taxaMutacao) {
                listaNova.add(new Gene());
            } else {
                listaNova.add(gene);
            }
        }
        novo.setGenes(listaNova);
        return novo;
    }

    @Override
    public IChromosome cross(IChromosome other) {
        Random rnd = new Random();
        Cromossoma novo = new Cromossoma(this);
        List<IGene> listaNova = null;
        int tamanho = 0;

        //tamanho
        if (other.getGenes().size() > this.getGenes().size()) {
            tamanho = this.getGenes().size();
        } else {
            tamanho = other.getGenes().size();
        }

        //gera um numero aleatorio entre 0 e 1
        //50% da vezes usa o gene deste cromossoma e outros 50% usa o gene do cromossoma other
        for (int i = 0; i < tamanho; i++) {
            if (rnd.nextDouble() < 0.5) {
                listaNova.add(this.genes.get(i));
            } else {
                listaNova.add(other.getGenes().get(i));
            }
        }
        novo.setGenes(listaNova);
        return novo;
    }

    @Override
    public IChromosome heredity() {
        return new Cromossoma(this);
    }

    @Override
    protected Object clone() {
        return new Cromossoma(this);
    }

    @Override
    public String toString() {
        String string_array_genes = "{" + genes.get(0);
        for (IGene gene : genes) {
            string_array_genes = string_array_genes + ", " + gene;
        }
        string_array_genes = string_array_genes + "}";
        return string_array_genes;
    }

    public int getMaxSize() {
        return this.maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getMinSize() {
        return this.minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    @Override
    public List<IGene> getGenes() {
        return this.genes;
    }

    public void setGenes(List<IGene> genes) {
        this.genes = genes;
    }

    public RunResult getResultado() {
        return this.resultado;
    }

    public void setResultado(RunResult resultado) {
        this.resultado = resultado;
    }

    public Integer[] getArrayGene() {
        Integer[] array = new Integer[genes.size()];

        for (int i = 0; i < genes.size(); i++) {
            array[i] = ((Gene) genes.get(i)).getGene();
        }

        return array;
    }

    @Override
    public int compareTo(IChromosome o) {
        //se o fitness do cromossoma recebido for maior, devolve 1
        //se o fitness do cromossoma recebido for menor, devolve -1
        if (this.getFitness() > o.getFitness()) {
            return -1;
        } else {
            return 1;
        }
    }
}
