package jenetic.interfaces;

/**
 * Created by Davide on 27/02/2018.
 */
public interface IGene {
    
    /**
     * determines if a given Gene contains a valid allele (value)
     * 
     * @return true if the allele is valid within the solution space
     */
    boolean isValid();
}
